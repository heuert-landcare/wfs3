# Copyright (c) Manaaki Whenua - Landcare Research. All rights reserved.

Please give attribution according to the license if using this software.

# Creative Commons License

![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Commons Attribution 4.0 International License")
This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)
