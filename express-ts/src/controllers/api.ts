"use strict";

import { Response, Request } from "express";

import { Wfs3 } from "../model/wfs3"

export const wfs3 = (req: Request, res: Response) => {
	const wfs = new Wfs3(req, res)
	wfs.get()
}