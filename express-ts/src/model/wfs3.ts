import { WebService } from "./webservice"
import { Collection, Extent, Link, XMLNode } from "./wfs3types"
import * as tp from "typed-promisify"
import request = require("request")
import { Response, Request, NextFunction } from "express";
import * as xpath from "xpath"
import * as sax from "sax"
import * as saxpath from "saxpath"
import * as _ from "lodash";
import * as bodyParser from "body-parser"
import { SaXPath } from "saxpath";
import { linkSync } from "fs";
import { extendAccessToken } from "fbgraph";
import { DOMParser } from "xmldom"
import * as path from "path";
const get = tp.promisify(request.get)
const domParser = new DOMParser;

export class Wfs3 extends WebService {
	protected wfs2Url: string
	protected capsPromise: Promise<any>
	protected collectionsPromise: Promise<Collection[]>
	constructor(req: Request, res: Response) {
		super(req, res)
		this.wfs2Url = this.params.wfs2
		this.capsPromise = get({
			url: this.wfs2Url,
			qs: {
				service: "WFS",
				request: "GetCapabilities",
				version: "2.0.0"
			}
		})
	}

	get() {
		const baseUrl = this.baseUrl
		if (baseUrl.match(/collections\/.+\/items\/.+(\.json)?$/)) {
			return this.getFeature()
		}
		if (baseUrl.match(/collections(\.json)?$/i)) {
			return this.getCapabilities()
		}
		if (baseUrl.match(/conformance$/i)) {
			return this.getConformance()
		}
		if (baseUrl.match(/collections\/.+\/items(\.json)?$/i)) {
			return this.getFeatures()
		}
		if (baseUrl.match(/collections\/.+(\.json)?$/i)) {
			return this.getCollection()
		}
		const collectionsUrl = (this.url + this.req.url).replace(/\/wfs[^?]*/i, "/wfs/collections")
		this.res.redirect(collectionsUrl)
	}
	getFeature(): void {
		// https://lris.scinfo.org.nz/services;key=619588ed391245328d9c58fb16558c44/wfs?service=WFS&
		// version=2.0.0&request=GetFeature&typeNames=lris.scinfo.org.nz:layer-48076&count=3&
		// srsName=epsg:4326&outputFormat=application/json&featureID=layer-48076.29735
		const name = this.getCollectionsName()
		const featureId = this.getFeatureId()
		const qs: any = { // query string for request
			service: "WFS",
			version: "2.0.0",
			request: "GetFeature",
			typeNames: name,
			count: this.params.count || 1,
			srsName: this.params.srsname || "epsg:4326",
			outputFormat: "application/json",
			resourceId: featureId
		}
		const headerKeys = Object.keys(this.req.headers)
			.filter(k => k.toLowerCase() !== "host")
			.filter(k => k.toLowerCase() !== "accept-encoding") // we don't want it gzipped!
		const headers: any = {}
		headerKeys.map((k: string) => headers[k] = this.req.headers[k])
		request({
			url: this.wfs2Url,
			encoding: null,
			gzip: false,
			qs,
			headers,
		}, (err, response, body: Buffer) => {
			const json = JSON.parse(body.toString())
			this.res.json(json.features[0])
		})// pipe(this.res)
	}
	getFeatureId(): string {
		const matches = this.req.url.match(/\/collections\/([^/?&]+)\/items\/([^/?&]+)/i)
		const featureId = matches[2]
		return featureId
	}
	async getCaps(): Promise<Collection[]> {
		const url = this.url
		this.makeCollectionsPromise()
		return this.collectionsPromise
	}
	makeCollectionsPromise(): Promise<Collection[]> {
		const url = this.url
		const promise: Promise<Collection[]> = new Promise<Collection[]>((resolve) => {
			this.capsPromise.then(res => {
				const saxParser = sax.createStream(true, {})
				const streamer = new saxpath.SaXPath(saxParser, "//FeatureTypeList/FeatureType")
				const now = (new Date).toISOString()
				const xmlDoc = new DOMParser().parseFromString(res.body)
				const select = xpath.useNamespaces({
					"default": "http://www.opengis.net/wfs/2.0",
					"ows": "http://www.opengis.net/ows/1.1"
				})
				// xmlDoc.us
				// http://www.opengis.net/wfs/2.0
				const featureTypes = select("//default:FeatureTypeList/default:FeatureType", xmlDoc)
				// featureTypes.map(ft => console.log(ft))
				// const a = xpath.evaluate(".//*", xmlDoc, null, XPathResult.ANY_TYPE, null)
				// console.log(featureTypes[0])
				// featureTypes.map(ft => console.log(ft.toString()))
				const collections: Collection[] = featureTypes.map((om: Document) => {
					// console.log(om.getElementsByTagName("Name")[0].textContent)
					// const om = domParser.parseFromString(xml, "application/xml")
					const names = om.getElementsByTagName("Name")
					const titles = om.getElementsByTagName("Title")
					const abstracts = om.getElementsByTagName("Abstract")
					const projections = om.getElementsByTagName("DefaultCRS")
					const lowerXY = <Document>select("//ows:LowerCorner", om)[0]
					const upperXY = <Document>select("//ows:UpperCorner", om)[0]
					let lower = [-180, -90]
					let upper = [180, 90]
					// console.log(xml)
					// console.log("bbox", upperXY.textContent)
					if (lowerXY) {
						lower = lowerXY.textContent.split(" ").map(n => Number(n))
						upper = upperXY.textContent.split(" ").map(n => Number(n))
						// upper = om.getElementsByTagNameNS("ows", "UpperCorner")[0].textContent.split(" ").map(n => Number(n))
					}
					const name = names[0].textContent
					const wfs2UrlEncoded = encodeURIComponent(this.wfs2Url)
					const link = `${url}/${name}/items` + (this.wfs2Url ? `?wfs2=${wfs2UrlEncoded}` : "")
					const links = [{
						href: link,
						rel: "item",
						type: "application/geo+json",
						title: titles[0].textContent
					}/* , {
					href: `INCOMPLETE ${name}.html`,
					rel: "describedBy",
					type: "",
					title: "SAMPLE ONLY - Not implemented"
				}*/]
					const bboxNumbers = lower.concat(upper)
					const extent = {
						crs: "http://www.opengis.net/def/crs/OGC/1.3/CRS84", // projections[0].textContent,
						spatial: bboxNumbers,
						trs: "http://www.opengis.net/def/uom/ISO-8601/0/Gregorian",
						temporal: [now, now] // 4(/2) now, pun intended! :-)
					}
					const crs = ["string"]
					return ({
						title: titles[0].textContent,
						name,
						description: abstracts[0].textContent,
						links: links,
						extent,
						crs
					})
				})
				resolve(collections)
			})
		})
		this.collectionsPromise = promise
		return promise
	}
	getCapabilities(): void {
		const baseUrl = this.baseUrl
		const req = this.req
		const res = this.res
		const params = this.params
		const port = req.socket.localPort === 80 ? "" : `:${req.socket.localPort}`
		// const link = `${url}/${name}/items` + (this.wfs2Url ? `?wfs2=${wfs2UrlEncoded}` : "")
		const wfs2UrlEncoded = encodeURIComponent(this.wfs2Url)
		const url = this.url + ".json" + (this.wfs2Url ? `?wfs2=${wfs2UrlEncoded}` : "")
		const capsPromise = this.getCaps()
		capsPromise.then((collections) => {
			const capabilities = {
				"links": [
					{
						"href": `${url}`,
						"rel": "self",
						"type": "application/json",
						"title": "this document"
					},
					{
						"href": `${url}`,
						"rel": "alternate",
						"type": "text/html",
						"title": "this document as HTML"
					},
					{
						"href": "http://schemas.example.org/1.0/foobar.xsd",
						"rel": "describedBy",
						"type": "application/xml",
						"title": "XML schema for Acme Corporation data"
					}
				],
				collections
			}
			res.json(capabilities)
		})
	}
	getFeatures(): void {
		const req = this.req
		const res = this.res
		const baseUrl = this.baseUrl
		const params = _.mapKeys(req.query, (v: string, k: string) =>
			k.toLowerCase()
		)
		const port = req.socket.localPort === 80 ? "" : `:${req.socket.localPort}`
		const url = `${req.protocol}://${req.hostname}${port}${baseUrl}`.replace(/\.json$/i, "")
		const collectionsName = this.getCollectionsName()
		if (!collectionsName) {
			// throw service exception as this is required
		}
		const limit = params.limit ? Number(params.limit) : undefined
		const bbox: number[] = params.bbox ? params.bbox.split(",").map(c => Number(c)) : undefined;
		const time: string | undefined = params.time

		// https://lris.scinfo.org.nz/services;key=619588ed391245328d9c58fb16558c44/wfs?service=WFS&version=2.0.0&request=GetFeature&typeNames=lris.scinfo.org.nz:layer-48076&count=3&srsName=epsg:4326&outputFormat=application/json
		// return res.end(`WFS3 class items: ${collectionsName}; limit: ${limit}; bbox: ${bbox}; time: ${time}`)
		return this.getCollections()
	}
	getCollection(): void {
		const name = this.getCollectionsName()
		this.makeCollectionsPromise().then(collections => {
			this.res.json(collections.filter(c => c.name === name)[0])
		})
	}
	getCollectionsName(): string {
		const matches = this.req.url.match(/\/collections\/([^/?&]+)/i)
		const name = matches[1]
		return name
	}

	getCollections(): void {
		// service=WFS&version=2.0.0&request=GetFeature&typeNames=lris.scinfo.org.nz:layer-48076&count=3&srsName=epsg:4326&outputFormat=application/json
		const name = this.getCollectionsName()
		// const myParams = `service=WFS&version=2.0.0&request=GetFeature&typeNames=${name}&count=3&srsName=epsg:4326&outputFormat=application/json`
		const bbox = this.params.bbox
		const outputFormat = this.params.outputformat || "application/vnd.geo+json application/json"
		const qs: any = { // query string for request
			service: "WFS",
			version: "2.0.0",
			request: "GetFeature",
			typeNames: name,
			count: this.params.count || 3,
			srsName: this.params.srsname || "epsg:4326",
			outputFormat
		}
		if (bbox) {
			qs.bbox = bbox
		}
		const headerKeys = Object.keys(this.req.headers)
			.filter(k => k.toLowerCase() !== "host")
		const headers: any = {}
		headerKeys.map((k: string) => headers[k] = this.req.headers[k])
		// console.log(headers)
		const qIndex = this.wfs2Url.indexOf("?")
		if (qIndex !== -1) {
			this.wfs2Url = this.wfs2Url.substring(0, qIndex)
		}
		request({
			url: this.wfs2Url,
			qs,
			headers,
		}).pipe(this.res)
	}
	getConformance(): void {
		const conformance = {
			"conformsTo": [
				"http://www.opengis.net/spec/wfs-1/3.0/req/core",
				"http://www.opengis.net/spec/wfs-1/3.0/req/oas30",
				"http://www.opengis.net/spec/wfs-1/3.0/req/html",
				"http://www.opengis.net/spec/wfs-1/3.0/req/geojson"
			]
		}
		this.res.json(conformance)
	}
}
