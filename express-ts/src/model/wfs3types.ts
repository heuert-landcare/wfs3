
export interface XMLNode {
	textContent: string
}
export interface Link {
	href: string
	rel: string
	type: string
	title: string
}
export interface Extent {
	crs: string
	spatial: number[]
	trs: string
	temporal: string[]
}
export interface Collection {
	name: string
	title: string
	description: string
	links: Link[]
	extent: Extent
	crs: string[]
}