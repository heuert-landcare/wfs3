import express = require("express");
import * as compression from "compression"; // compresses requests
import * as bodyParser from "body-parser";
// import * as logger from "./util/logger";
import * as lusca from "lusca";
import * as dotenv from "dotenv";
import * as path from "path";
import * as expressValidator from "express-validator";

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: ".env.example" });

// Controllers (route handlers)
import * as apiController from "./controllers/api";

// Create Express server
const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});


app.use(express.static(path.join(__dirname, "public"), { maxAge: 31557600000 }))

app.get(/\/wfs.*/i, apiController.wfs3)

export default app;