
// declare function multistream(streams: Stream[]): Stream
// declare module 'multistream': (streams: Stream[]) => Stream;
// export as namespace multistream;


/*
declare module 'multistream' {
	import { Stream } from 'stream';
	// const multistream: (streams: Stream[]) => Stream;
	function multistream(streams: Stream[]): Stream;
	export default multistream;
}
*/

/*
declare module 'multistream' {
	// import { Stream } from 'stream';
	// export default function multistream(a:any):any;
	function MultiStream(a:any):any;
	export default MultiStream;
}
*/

interface Options {
	parameters: any[]
	schemas: any
	version: string
	errorTransformer: any
	customFormats: any
}

declare module "openapi-request-validator" {
	// import { Stream } from 'stream';
	// export default function multistream(a:any):any;
	function DefaultConstructor(a: any): any
	export default DefaultConstructor
}