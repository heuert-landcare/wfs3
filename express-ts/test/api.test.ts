import request = require("supertest")
import app from "../src/app";
import * as expect from "expect"

const wfs2 = "https://lris.scinfo.org.nz/services;key=619588ed391245328d9c58fb16558c44/wfs"
const age = 24 * 3600 // 24 hours
describe("/wfs/collections", () => {
	it("gets capabilities", () => {
		const wfsReq = request(app).get(`/wfs/collections?wfs2=${wfs2}`)
		wfsReq.set("Cache-Control", `max-age=${age}`)
		wfsReq.expect(200)
	})
});

describe("/wfs/collections/lris.scinfo.org.nz:layer-95527/items", () => {
	it("gets 3 items", () => {
		const wfsReq = request(app).get(`/wfs/collections/lris.scinfo.org.nz:layer-95527/items?wfs2=${wfs2}`)
		wfsReq.set("Cache-Control", `max-age=${age}`)
		wfsReq.expect(200)
			.then(response => {
				expect(response.body.type).toBe("FeatureCollection")
				expect(response.body.features.length).toBe(3)
			})
	})
	it("gets n items", () => {
		const wfsReq = request(app).get(`/wfs/collections/lris.scinfo.org.nz:layer-95527/items?wfs2=${wfs2}&count=5`)
		wfsReq.set("Cache-Control", `max-age=${age}`)
		wfsReq.expect(200)
			.then(response => {
				expect(response.body.features.length).toBe(5)
			})
	})
});

describe("/wfs/collections/lris.scinfo.org.nz:layer-95527", () => {
	it("has valid JSON", async () => {
		jest.setTimeout(20000)
		const wfsReq = request(app).get(`/wfs/collections/lris.scinfo.org.nz:layer-95527?wfs2=${wfs2}`)
		wfsReq.set("Cache-Control", `max-age=${age}`)
		wfsReq.expect(200)
		const response = await wfsReq
		const body: any = response.body
		expect(typeof body).toBe("object")
		expect(body.name).toBe("lris.scinfo.org.nz:layer-95527")
		expect(typeof body.title).toBe("string")
		expect(body.links instanceof Array).toBeTruthy()
		expect(typeof body.extent).toBe("object")
		expect(typeof body.crs).toBe("object")
	})
})

describe("/wfs/collections/lris.scinfo.org.nz:layer-95527/items/layer-95527.1", () => {
	it("has valid JSON", async () => {
		jest.setTimeout(20000)
		const wfsReq = request(app).get(`/wfs/collections/lris.scinfo.org.nz:layer-95527/layer-95527.1?wfs2=${wfs2}`)
		wfsReq.set("Cache-Control", `max-age=${age}`)
		wfsReq.expect(200)
		const response = await wfsReq
		const body: any = response.body
		expect(typeof body).toBe("object")
		/*
		expect(body.name).toBe("lris.scinfo.org.nz:layer-95527")
		expect(typeof body.title).toBe("string")
		expect(body.links instanceof Array).toBeTruthy()
		expect(typeof body.extent).toBe("object")
		expect(typeof body.crs).toBe("object")
		*/
	})
})