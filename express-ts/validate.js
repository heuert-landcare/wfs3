var OpenapiRequestValidator = require('openapi-request-validator');
var validator = new OpenapiRequestValidator({
  parameters: [
    {
      in: 'query',
      type: 'string',
      name: 'foo',
      required: true
    }
  ],
  schemas: null, // an optional array or object of jsonschemas used to dereference $ref
  version: 'swagger-2.0', // default optional value for future versions of openapi
  errorTransformer: null, // an optional transformer function to format errors
  customFormats: {
    // support `"format": "foo"` for types.
    foo: function(input) {
      return input === 'foo';
    }
  }
});

var request = {
  body: {},
  params: {},
  query: {foo: 'wow', bar: 'baz'}
};
var errors = validator.validate(request);
console.log(errors); // => null