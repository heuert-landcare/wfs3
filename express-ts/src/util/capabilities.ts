import fs = require("fs")
import sax = require("sax")
// import saxpath = require("saxpath")
import { SaXPath } from "saxpath"


export const parseCapabilities = () => {
	// const saxStream = sax.createStream(true, {})
	const fileStream = fs.createReadStream("../earth_observations_wms.xml")

	/*
	saxStream.on("text", (node) => {
		if (true || node.name === "Dimension") {
			console.log(node)
		}
	})
	*/
	// xmlStream.pipe(saxStream)
	const saxParser  = sax.createStream(true, {})
	const streamer = new SaXPath(saxParser, "//Dimension")
	streamer.on("match", (xml) => {
		console.log(xml)
	})
	fileStream.pipe(saxParser)
}
