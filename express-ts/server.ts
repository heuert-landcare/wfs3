// import * as https from 'https'
import * as fs from 'fs'
import { DOMParser } from 'xmldom'
import * as xpath from 'xpath'

const file = '../earth_observations_wms.xml'
const encoding = 'utf8'
const select = xpath.useNamespaces({w: 'http://www.opengis.net/wms'});
const url = 'https://neo.sci.gsfc.nasa.gov/wms/wms?version=1.3.0&service=WMS&request=GetMap&layers=MOD11C1_E_LSTDA&crs=CRS:84&bbox=0,10,60,50&width=1920&height=1080&format=image/png&time=2000-02-26T00:00:00'
function expand(time: string[]) {
	const period = time.length === 3 ? time[2] : ''
	const start = time[0]
	const end = period ? time[1] : start
	const days = period ? Number(period.substring(1, 2)) : 0
	console.log(days)
	return {
		start: new Date(start),
		end: new Date(end),
		days
	}
}
fs.readFile(file, encoding, (err, text) => {
	const parser = new DOMParser()
	const doc = parser.parseFromString(text, 'application/xml')
	const dimensionNode = <Element> select('//w:Name[text()="MOD11C1_E_LSTDA"]/../w:Dimension[@name="time"]', doc, true)
	const timesString = dimensionNode.childNodes[0].nodeValue
	// console.log(dimensionNode.childNodes[0].nodeValue)
	const times = timesString.split(',').map(interval => interval.split('/'))
	// console.log(times)
	const intervals = times.map(t => expand(t))
	console.table(intervals)
})


